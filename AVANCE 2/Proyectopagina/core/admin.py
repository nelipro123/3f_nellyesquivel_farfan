from django.contrib import admin
from .models import Cuenta, Cliente, Pago, Categoria, Coleccion, Producto, Proveedor, Orden

@admin.register(Cuenta)
class CuentaAdmin(admin.ModelAdmin):
    list_display = [
        "usuario",
        "email",
        "contrasena",
    ]

@admin.register(Cliente)
class ClienteAdmin(admin.ModelAdmin):
    list_display = [
        "numero",
        "nombre",
        "apellido_paterno",
        "apellido_materno",
        "contacto",
        "cuenta",
    ]

@admin.register(Pago)
class PagoAdmin(admin.ModelAdmin):
    list_display = [
        "numero",
        "fecha",
        "total",
        "descripcion",
    ]

@admin.register(Categoria)
class CategoriaAdmin(admin.ModelAdmin):
    list_display = [
        "numero",
        "nombre",
    ]

@admin.register(Coleccion)
class ColeccionAdmin(admin.ModelAdmin):
    list_display = [
        "codigo",
        "nombre",
        "descripcion",
        "categoria",
    ]

@admin.register(Producto)
class ProductoAdmin(admin.ModelAdmin):
    list_display = [
        "codigo",
        "nombre",
        "descripcion",
        "precio",
        "coleccion",
    ]

@admin.register(Proveedor)
class ProveedorAdmin(admin.ModelAdmin):
    list_display = [
        "numero",
        "nombre",
        "apellido_paterno",
        "celular",
        "email",
    ]

@admin.register(Orden)
class OrdenAdmin(admin.ModelAdmin):
    list_display = [
        "codigo",
        "fecha_inicial",
        "fecha_final",
        "calle",
        "codigo_postal",
        "colonia",
        "cliente",
        "pago",
    ]