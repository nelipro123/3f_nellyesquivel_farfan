
from django import forms
from .models import Cuenta, Cliente, Pago, Categoria, Coleccion, Producto, Proveedor, Orden

# Actualiza ProductForm
class ProductForm(forms.ModelForm):
    class Meta:
        model = Producto  # Actualiza el modelo a Producto
        fields = [
            "nombre",
            "descripcion",
            "precio",
            "coleccion",
        ]  # Actualiza los campos para que coincidan con el nuevo modelo
        widgets = {
            "nombre": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Escribe el nombre del Producto"}),
            "descripcion": forms.Textarea(attrs={"type": "text", "class": "form-control", "rows": 3, "placeholder": "Escribe la descripcion del Producto"}),
            "precio": forms.TextInput(attrs={"type": "number", "class": "form-control"}),
            "coleccion": forms.Select(attrs={"class": "form-select form-control"}),
        }

# Actualiza UpdateProductForm
class UpdateProductForm(forms.ModelForm):
    class Meta:
        model = Producto  # Actualiza el modelo a Producto
        fields = [
            "nombre",
            "descripcion",
            "precio",
            "coleccion",
        ]  # Actualiza los campos para que coincidan con el nuevo modelo
        widgets = {
            "nombre": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Escribe el nombre del Producto"}),
            "descripcion": forms.Textarea(attrs={"type": "text", "class": "form-control", "rows": 3, "placeholder": "Escribe la descripcion del Producto"}),
            "precio": forms.TextInput(attrs={"type": "number", "class": "form-control"}),
            "coleccion": forms.Select(attrs={"class": "form-select form-control"}),
        }