from django.db import models

from django.contrib.auth.models import User
# Create your models here.

# Modelo para la tabla Cuenta
class Cuenta(models.Model):
    usuario = models.CharField(max_length=50, primary_key=True)
    email = models.EmailField()
    contrasena = models.CharField(max_length=128)

    def __str__(self):
        return self.cuenta_name

# Modelo para la tabla Clientes
class Cliente(models.Model):
    numero = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=50)
    apellido_paterno = models.CharField(max_length=50)
    apellido_materno = models.CharField(max_length=50)
    contacto = models.CharField(max_length=100)
    cuenta = models.ForeignKey(Cuenta, on_delete=models.CASCADE)

    def __str__(self):
        return self.cliente_name

# Modelo para la tabla Pagos
class Pago(models.Model):
    numero = models.AutoField(primary_key=True)
    fecha = models.DateField()
    total = models.DecimalField(max_digits=10, decimal_places=2)
    descripcion = models.TextField()

    def __str__(self):
        return self.pago_name

# Modelo para la tabla Categorias
class Categoria(models.Model):
    numero = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=50)

    def __str__(self):
        return self.categoria_name

# Modelo para la tabla Colecciones
class Coleccion(models.Model):
    codigo = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=50)
    descripcion = models.TextField()
    categoria = models.ForeignKey(Categoria, on_delete=models.CASCADE)

    def __str__(self):
        return self.coleccion_name

# Modelo para la tabla Productos
class Producto(models.Model):
    codigo = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=100)
    descripcion = models.TextField()
    precio = models.DecimalField(max_digits=10, decimal_places=2)
    coleccion = models.ForeignKey(Coleccion, on_delete=models.CASCADE)

    def __str__(self):
        return self.producto_name

# Modelo para la tabla Proveedores
class Proveedor(models.Model):
    numero = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=50)
    apellido_paterno = models.CharField(max_length=50)
    celular = models.CharField(max_length=20)
    email = models.EmailField()

    def __str__(self):
        return self.proveedor_name

# Modelo para la tabla Ordenes
class Orden(models.Model):
    codigo = models.AutoField(primary_key=True)
    fecha_inicial = models.DateField()
    fecha_final = models.DateField()
    calle = models.CharField(max_length=100)
    codigo_postal = models.CharField(max_length=10)
    colonia = models.CharField(max_length=50)
    cliente = models.ForeignKey(Cliente, on_delete=models.CASCADE)
    pago = models.ForeignKey(Pago, on_delete=models.CASCADE)

    def __str__(self):
        return self.orden_name