from django.shortcuts import render

from django.views import generic
from django.urls import reverse_lazy

from django.http import HttpResponse
from .models import Cuenta, Cliente, Pago, Categoria, Coleccion, Producto, Proveedor, Orden
from .forms import ProductForm, UpdateProductForm

# Create your views here.

## Create
class CreateProducto(generic.CreateView):
    template_name = "core/create_product.html"
    model = Producto
    form_class = ProductForm
    success_url = reverse_lazy("core:list_producto")


## Retrieve
# List
class ListProduct(generic.ListView):
    template_name = "core/list_product.html"
    model = Producto
    context_object_name = "productos"

# Detail
class DetailProduct(generic.DetailView):
    template_name = "core/detail_product.html"
    model = Producto
    context_object_name = "producto"


## Update
class UpdateProduct(generic.UpdateView):
    template_name = "core/update_product.html"
    model = Producto
    form_class = UpdateProductForm
    success_url = reverse_lazy("core:list_producto")

## Delete
class DeleteProduct(generic.DeleteView):
    template_name = "core/delete_product.html"
    model = Producto
    success_url = reverse_lazy("core:list_producto")