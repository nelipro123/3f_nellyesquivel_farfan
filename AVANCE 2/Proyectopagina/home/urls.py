from django.urls import path

from home import views


app_name = "home"

urlpatterns = [
    path('', views.Index.as_view(), name="index"),
    path('house_of_cards/', views.HouseOfCardsView.as_view(), name="houseofcards"),
    path('colecciones/', views.ColeccionesView.as_view(), name="colecciones"),
]