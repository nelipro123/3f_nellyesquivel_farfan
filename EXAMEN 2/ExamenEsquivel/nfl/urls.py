from django.urls import path 
from . import views 

urlpatterns = [
    path('', views.inicio, name='inicio' ),

    path('nosotros', views.nosotros, name='nosotros'),

    path('equipos', views.equipos, name='equipos'),

    path('equipos/crear_equipo', views.crear_equipo, name='crear_equipo'),

    path('equipos/editar_equipo', views.editar_equipo, name='editar_equipo'),
]