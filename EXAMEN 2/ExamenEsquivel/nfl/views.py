from django.shortcuts import render
from django.http import HttpResponse
from .models import Equipo

# Acceso a seccion de pagina

def inicio(request):
    return render(request, 'paginas/inicio.html')

def nosotros(request):
    return render(request, 'paginas/nosotros.html')

    #Acceso a seccion de equipos

def equipos(request):
    return render(request, 'equipos/index.html', )

def crear_equipo(request):
    return render(request, 'equipos/crear_equipo.html')

def editar_equipo(request):
    return render(request, 'equipos/editar_equipo.html')